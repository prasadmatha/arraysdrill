function find(elements,cb){
    if (Array.isArray(elements) && elements.length > 0 && cb !== undefined && cb.constructor.name === "Function"){
        for (let element of elements){
            if (cb(element)){
                return element
            }
        }
        return undefined
    }
    return []
    
}


module.exports = find
