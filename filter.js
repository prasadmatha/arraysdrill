function filter(elements,cb){
    if (Array.isArray(elements) && elements.length > 0 && cb !== undefined && cb.constructor.name === "Function"){
        let filteredArray = []
        let result
        for (let index in elements){
          result  = cb(elements[index],index,elements)
          if (result === true){
              filteredArray.push(elements[index])
          }
        }
        if (filteredArray.length === 0){
            return []
        }
        return filteredArray
    }
    return []
    
}


module.exports = filter
