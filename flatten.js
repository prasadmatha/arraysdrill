function flatten(array,depth=1){
    if (Array.isArray(array) && array.length > 0 && depth > 0){
        let count = 0
        let isNested;
        function flat(array,depth){
            isNested = false
            let result = []
            //[1, [2], [[3]], [[[4]]]]
            for (let index = 0; index < array.length ; index++){
                let currentItem = array[index]
                if (Array.isArray(currentItem)){
                    result = result.concat(currentItem)
                    isNested = true
                }
                else if(currentItem === undefined || currentItem === null){
                    continue
                }   
                else{
                    
                    result.push(currentItem)
                }
            }
            
            if(isNested){
                count += 1
            }
            array = result
            result = []
    
            if (isNested === false|| count === depth){                
                return array
            }
            else{
                return flat(array,depth)
            }
        }
        return flat(array,depth)
        
    }
    else if (Array.isArray(array) && array.length > 0 && depth === 0){
        return array
    }
    else{
        return []
    }
}


module.exports = flatten