function map(elements,cb){
    if (Array.isArray(elements) && elements.length > 0 && cb !== undefined && cb.constructor.name === "Function"){
        let newArray = []
        for (let index=0; index < elements.length ; index++){
           
            newArray.push(cb(elements[index],index,elements))
            
            
        }
        return newArray
    }
    return []
    
}

module.exports = map

