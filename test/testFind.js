function cb(element){
    return (element % 2 === 0)
}

const items = require("../arrays")

const find = require("../find")

const result = find(items,cb)

console.log(result)
