function squares(element,index,array){
    if (element !== undefined){
        return element ** 2
    }
}

function doubles(element,index,array){
    if (element !== undefined){
        return element * 2
    }
}

function sqrt(element,index,array){
    if (element !== undefined){
        return (Math.sqrt(element))
    }
}

function ParseInt(element,index,array){
    if (element !== undefined){
        return parseInt(element,index)
    }
     
}

const callback = (number, index,array) => {

    return (number * number) + index
}


const items = require("../arrays")

const map = require("../map")


const result = map(items,callback)

console.log(result)
