function totalSum(previousValue,element,index,array){
    let sum = previousValue + element
    return sum
}
//previousValue, currentValue, currentIndex, array

const items = require("../arrays")

const reduce = require("../reduce")

const result = reduce(items,totalSum,0)

console.log(result)
